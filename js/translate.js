function translate(language) {
  fetch("js/languages.json")
    .then(response => response.json())
    .then(data => {
      for (id of Object.keys(data)) {
        // Use English if the message is not translated
        var text = Object.keys(data[id]).includes(language)? data[id][language] : data[id]['en'];
        var elm = document.getElementById(id) || document.getElementsByClassName(id);
        if (elm.length)
          for (var j = 0; j < elm.length; j++)
            elm[j].innerHTML = text;
        else
          elm.innerHTML = text;
      }
      document.querySelector('[data-language="' + language + '"]').classList.add("active");
      var langs = ['en', 'pt', 'es', 'zh', 'uk', 'nl', 'eu'];
      for (i = 0; i < langs.length; i++)
        if (language != langs[i]) {
          var elm = document.querySelector('[data-language="' + langs[i] + '"]');
          if (elm) // maybe the language selection doesn't include this one'
            elm.classList.remove("active");
        }
    })
    .catch(err => console.log(err))
}
